package soft4;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * tower + roadblock
 * @author Povazson
 *
 */
public class TowerPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6070736480494279785L;
	private TowerList tl;
	private ImageIcon torony, block;
	private Player player;
	
	public TowerPanel(TowerList list,Player p){
		super();
		player = p;
		tl = list;
		Color bckgrnd = new Color(255,0,0,0);
		setBackground(bckgrnd);
		setOpaque(false);
		setSize(512,512);
		
		torony = new ImageIcon(new Tower().getPicture());
		block = new ImageIcon(new RoadBlock().getPicture());
		
	}
	
	private void update(){
		removeAll();
		int max = tl.getTowerNumber();
		for (int i=0; i<max; i++){
			JLabel temp = new JLabel(torony);
			temp.setBounds(tl.getTower(i).getLocation().x*32,tl.getTower(i).getLocation().y*32,32,32);
			add(temp);		
		}		
		
		max = player.getRoadBlockNumber();
		for (int i=0; i<max; i++){
			JLabel temp = new JLabel(block);
			temp.setBounds(player.getRoadBlock(i).getLocation().x*32,player.getRoadBlock(i).getLocation().y*32,32,32);
			add(temp);		
		}		
	}
	
	@Override
	public void paintComponent(Graphics g){
		update();
		super.repaint();
	}
	
	@Override
	public Dimension getPreferredSize(){
		return new Dimension(512,512);
	}
}
