package soft4;

/**
 * 
 * Az ellens�gek k�z�s �se, tartalmazza azok k�z�s <code>move</code> met�dus�t.
 * @author Povazson
 */
public abstract class Creep implements Cloneable{
	/**
	 * azon routeSegment referenci�ja, amelyiken �pp tart�zkodik
	 */
	protected RouteSegment activeSegment; 
	/**
	 * az ellens�g �letpontjai.
	 */
	protected int hp;
	/**
	 * k�vetkez� l�p�s ideje, ha 0 akkor l�p.
	 */
	protected int nextMoveIn;
	/**
	 * a tartalmaz� lista referenci�ja
	 */
	protected CreepList context;
	
	/**
	 * Konstruktor.
	 * @param ctx a tartalmaz� lista referenci�ja
	 * @param rts a kiindul�si �tszegmens referenci�ja
	 * @param tts az id�, mire elindul
	 */
	public Creep(int h, CreepList ctx, RouteSegment rts, int tts) {
		context = ctx;
		activeSegment = rts;
		nextMoveIn = tts;
		hp = h;
	}
	
	/**
	 * A sebz�s szignat�r�ja.
	 * A lesz�rmazottaknak meg kell val�s�taniuk, hacsak nem absztraktak maguk is.
	 * @param damage az alap sebz�s
	 * @param ammo a sebz�sm�dos�t� k� referenci�ja
	 */
	public abstract void hurt(int damage, DamageModifierStone ammo);


	public void setSpeed(int s){
		nextMoveIn = s;
	}

	/**
	 * Az ellens�gek mozg�sa.
	 * Akkor l�p ha <code>nextMoveIn</code> �rt�ke 0, k�l�nben ez �rt�k�t cs�kkenti.
	 * Ha az ellens�g el�rt az utols� �tszegmensre, akkor a j�t�kos vesz�t.
	 */
	public void move(){
		nextMoveIn--;
		if(nextMoveIn<=0){
			RouteSegment tmp = activeSegment.onMove(this);
			if(tmp==null){
				context.getPlayer().defeat();
				return;
			}
			activeSegment = tmp;
			nextMoveIn = activeSegment.getSpeed();
		}
	}
	
	/**
	 * 
	 * @return k�p neve
	 */
	public abstract String getPicture();
	
	/**
	 * 
	 * @return melyik szegmensen van
	 */
	public RouteSegment getLocation(){
		return activeSegment;
	}
	
	/**
	 * 
	 * @return mennyi man�t ad, ha meghal
	 */
	public abstract int getMana();
	
}