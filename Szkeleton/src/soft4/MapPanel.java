package soft4;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * A j�t�k p�ly�j�nak kirajzol�sa, a mez�k�n val� kattint�s kezel�se
 * @author Povazson
 *
 */
public class MapPanel extends JPanel implements MouseListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6287989923774648155L;
	private ArrayList<Field> fields;
	private JLabel[][] labels;
	private JLabel active;
	private int size;
	private HashMap<String,ImageIcon> pictures;
	private Player player;
	
	public MapPanel(Player p){
		super();
		active = null;
		setSize(512,512);
		setMaximumSize(new Dimension(512,512));
		fields = p.getMapData().getMap();
		Field hegy = p.getMapData().getMountain();
		pictures = new HashMap<>();
		size = p.getMapData().getSize();
		labels = new JLabel[size][size];
		player = p;
		setLayout(new GridLayout(size, size,0,0));
		JLabel tmp;
		for(Field f:fields){
			if(f==hegy){
				tmp = new JLabel(new ImageIcon("mountain.png"));
				labels[hegy.x][hegy.y]=tmp;
				add(tmp);
			}
			else {
			if(!pictures.containsKey(f.getPicture())){
				pictures.put(f.getPicture(), new ImageIcon(f.getPicture()));
			}
			tmp = new JLabel(pictures.get(f.getPicture()));
			tmp.addMouseListener(this);
			labels[f.x][f.y] = tmp;
			add(tmp);
			}
		}
	}
	
	@Override
	public Dimension getPreferredSize(){
		return new Dimension(512,512);
	}
	@Override
	public void mouseClicked(MouseEvent e) {
	}
	@Override
	public void mouseEntered(MouseEvent e) {
	}
	@Override
	public void mouseExited(MouseEvent e) {
	}
	@Override
	public void mousePressed(MouseEvent e) {
		if(active!=null)active.setBorder(BorderFactory.createEmptyBorder());
		int x = 0; 
		int y = 0;
		for(int i=0;i<size;i++){
			for(int j=0;j<size;j++){
				if(labels[i][j]==e.getSource()){
					y = i;
					x = j;
					active = labels[i][j];
					labels[i][j].setBorder(BorderFactory.createLineBorder(Color.YELLOW, 1));
				}
			}
		}
		player.setActiveField(fields.get(x*size+y)); 
	}
	@Override
	public void mouseReleased(MouseEvent e) {
	}
	
	/**
	 * akt�v szelekci� t�rl�se
	 */
	public void clearActive(){
		if(active!=null)active.setBorder(BorderFactory.createEmptyBorder());
		active = null;
	}
}
