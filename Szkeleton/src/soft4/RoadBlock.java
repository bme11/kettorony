package soft4;

/**
 * Road-ra �p�thet�, creep-et lass�t, a player �p�t�. K� rakhat� bele,
 *  amivel tov�bbi lass�t�s �rhet� el. A lass�t�s az �sszes, roadblock-al �rintkez� creep-re �rv�nyes�l.
 *
 */
public class RoadBlock {
	public static final int cost = 30;
	public static final int speed = 60;
	/**
	 * a roadblock-ban l�v� k�
	 */
	private RoadBlockStone stone;
	/**
	 * az az �t, melyen tal�lhat�,
	 * l�that�s�g miatt
	 */
	private Road road;
	
	//picture lek�rdez�shez
	public RoadBlock(){
		
	}
	
	/**
	 * Konstruktor	
	 * @param r az akad�lyt tartalmaz� road p�ld�ny
	 */
	public RoadBlock(Road r) {
		road = r;
	}

	/**
	 * Hib�t ad vissza, ha m�r van stone a roadblockban, egy�k�nt a param�terk�nt kapott �rt�k be�ll�t�sa a stone attrib�tumba.	
	 * @param rs a hozz�rendelend� roadblockstone p�ld�ny
	 * @return <code>true</code>, ha m�g nincs  benne k�, <code>false</code> ha m�r van
	 */
	public boolean setStone(RoadBlockStone rs){
		if(stone==null){
			stone = rs;
			stone.setRoadAttribute(road);
			return true;
		}
		else {
			return false;
		}
	}
	public String getPicture(){
		return "roadblock.png";
	}
	
	public Field getLocation(){
		return road;
	}
}
