package soft4;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
/**
 * A p�lya adatainak t�rol�sa
 * @author Povazson
 *
 */
public class MapData {
	/**
	 * a p�lya l�that� elemei
	 */
	private ArrayList<Field> fields;
	
	/**
	 * az ellens�gek sz�m�ra haszn�lhat� �tvonal els� szegmense
	 */
	private RouteSegment startSegment;
	
	/**
	 * a map egy oldal�nak m�rete
	 */
	private int size;
	
	/**
	 * toronylista, �t kell adni a buildsiteoknak konstukt�l�skor
	 */
	private TowerList twrlst;
	
	/**
	 * �t kell adni a routesegmenteknek generl�skor
	 */
	private Weather weather;
	
	private Field hegy;
	
	/**
	 *  Warning n�k�li ford�t�s �rdek�ben kiszedve,
	 *  mivel a szkeletonban m�g nem haszn�ljuk
	 */
//	private Player player;
	
	/**
	 * Konstruktor.
	 * Az adatok alaphelyzetbe �ll�t�sa az <code>initialize</code> met�dusban t�rt�nik.
	 * @see #initialize()
	 */
	public MapData(TowerList tl, Weather w) {
		fields = new ArrayList<>();
		startSegment = null;
		twrlst = tl;
		weather = w;
		initialize("map_data.dat","route_data.dat","adjacency.dat");
	}
	
	/**
	 * A p�lya adatainak inicializ�l�sa.
	 * F�jlokb�l olvassa be a p�lya szerkezet�t, valamint az �tvonalakat.
	 * @param map a p�lya elrendez�s�t tartalmaz� f�jl neve
	 * @param route a p�lya �tvonalait tartalmaz� f�jl neve
	 * @param adjacency a p�lya mez�inek szomszedoss�g�t tartalmaz� f�jl neve
	 */
	public int initialize(String map, String route, String adjacency){
		BufferedReader br = null;
		FileInputStream fis;
		String line;
		fields.clear();
/**
 * mapt�lt�s...
 */
		try {
			fis = new FileInputStream(map);
			br = new BufferedReader(new InputStreamReader(fis));
			boolean isSet = false;
			Field f;
			while((line = br.readLine())!=null){
				if(!isSet){
					size=line.length();
					isSet = true;
				}
				for(int i=0;i<=size-1;i++){
					if(line.charAt(i)=='1'){
						f = new Road(40);
					}else{
						f = new BuildSite(twrlst);
					}
					f.x = fields.size()%size;
					f.y = fields.size()/size;
					fields.add(f);
				}
			}
			try {
				br.close();
			} catch (IOException e) {
				return 1;
			}			
		} catch (FileNotFoundException e) {
			return 1;
		} catch (IOException e) {
			return 1;
		}
		
/**
 * szomsz�doss�g�ll�t�s...
 */
		try {
			fis = new FileInputStream(adjacency);
			br = new BufferedReader(new InputStreamReader(fis));
			while((line = br.readLine())!=null){
				String[] nbors = line.split(":");
				int x = (int)Math.floor(Integer.parseInt(nbors[0])/100);
				int y = Integer.parseInt(nbors[0])%100;
				Field f = fields.get(y*size+x);
				for(int i = 1;i<nbors.length;i++){
					x = (int)Math.floor(Integer.parseInt(nbors[i])/100);
					y = Integer.parseInt(nbors[i])%100;
					f.addNeighbour(fields.get(y*size+x));
				}
			}
			try {
				br.close();
			} catch (IOException e) {
				return 1;				
			}			
		} catch (FileNotFoundException e) {
			return 1;
		} catch (IOException e) {
			return 1;
		}
		
/**
 * routesegments...		
 */
		
		try {
			fis = new FileInputStream(route);
			br = new BufferedReader(new InputStreamReader(fis));
			line = br.readLine();
			Point p = new Point(0,0);
			RouteSegment rs;
			RouteSegment rs2=null;
			ArrayList<RouteSegment> tmp = new ArrayList<>();
			
			int x = (int)Math.floor(Integer.parseInt(line)/100);
			int y = Integer.parseInt(line)%100;
			Field start = fields.get(y*size+x);
			hegy = null;
			
			//hegykeres�s 
			while((line = br.readLine())!=null){
				String[] nexts = line.split(":");
				x = (int)Math.floor(Integer.parseInt(nexts[0])/100);
				y = Integer.parseInt(nexts[0])%100;
				Field f = fields.get(y*size+x);
				Field f2 = null;
				for(int i = 1;i<nexts.length;i++){
					x = (int)Math.floor(Integer.parseInt(nexts[i])/100);
					y = Integer.parseInt(nexts[i])%100;
					f2 = fields.get(y*size+x);
				}
				if(f2 == null){
					hegy = f;
					rs2 = new RouteSegment(f,f,new Point(0,0),weather);
					tmp.add(rs2);
				}
			}
			try {
				br.close();
			} catch (IOException e) {
				return 1;
			}
			fis = new FileInputStream(route);
			br = new BufferedReader(new InputStreamReader(fis));
			br.readLine();
			//legy�rtja a tmp be a routesegmenteket
			while((line = br.readLine())!=null){
				String[] nexts = line.split(":");
				x = (int)Math.floor(Integer.parseInt(nexts[0])/100);
				y = Integer.parseInt(nexts[0])%100;
				Field f = fields.get(y*size+x);
				Field f2 = null;
				RouteSegment rs3 = null;
				
				for(int i = 1;i<nexts.length;i++){
					x = (int)Math.floor(Integer.parseInt(nexts[i])/100);
					y = Integer.parseInt(nexts[i])%100;
					f2 = fields.get(y*size+x);
					rs = null; rs2 = null;
					
					p.setLocation(0, 0);
					
					//a 0,0 ofszet� csak 1 fieldre mutat
					if(rs3==null){
						rs3 = new RouteSegment(f,f,p,weather);
						tmp.add(rs3);
						}
					
					for(int j = 1;j<8;j++){
						if(f2!=hegy){
						//le
						if(fields.indexOf(f2)==fields.indexOf(f)+size){
							p.translate(0, 1);
						}
						//fel
						else if(fields.indexOf(f2)==fields.indexOf(f)-size){
							p.translate(0, -1);
						}
						//jobbra
						else if(fields.indexOf(f2)==fields.indexOf(f)+1){
							p.translate(1, 0);
						}
						//balra
						else if(fields.indexOf(f2)==fields.indexOf(f)-1){
							p.translate(-1, 0);
						}
						rs = rs2;
						rs2 = new RouteSegment(f,f2,p,weather);
						if(j==1) rs3.addNext(rs2);
						else if(rs!=null) rs.addNext(rs2);
						tmp.add(rs2);
						}
					}
				}
				if(f2 == null){
					rs2 = new RouteSegment(f,f,new Point(0,0),weather);
					tmp.add(rs2);
				}
				
			}
			
			try {
				br.close();
			} catch (IOException e) {
				return 1;
			}
			fis = new FileInputStream(route);
			br = new BufferedReader(new InputStreamReader(fis));
			br.readLine();
			
			//be�ll�tja a fieldek rs pointer�t
			for(RouteSegment r: tmp){
				for(Field f:fields){
					if(r.getStart()==f||r.getEnd()==f)  f.addRouteSegment(r);
				}
			}
			
			//az �sszes �tir�nyban az utols� offszetre be�ll�tja nextre a k�vetkez� els�j�t.
			while((line = br.readLine())!=null){
				String[] nexts = line.split(":");
				x = (int)Math.floor(Integer.parseInt(nexts[0])/100);
				y = Integer.parseInt(nexts[0])%100;
				Field f = fields.get(y*size+x);
				for(int i = 1;i<nexts.length;i++){
					x = (int)Math.floor(Integer.parseInt(nexts[i])/100);
					y = Integer.parseInt(nexts[i])%100;
					Field f2 = fields.get(y*size+x);
					RouteSegment rtmp=null, rtmp2 = null;
					for(RouteSegment r : tmp){
						if(r.getStart()==f&&r.getEnd()==f2){
							//le
							if(fields.indexOf(f2)==fields.indexOf(f)+size){
								if(r.getOffset().equals(new Point(0,7))) rtmp = r;
							}
							//fel
							else if(fields.indexOf(f2)==fields.indexOf(f)-size){
								if(r.getOffset().equals(new Point(0,-7))) rtmp = r;
							}
							//jobbra
							else if(fields.indexOf(f2)==fields.indexOf(f)+1){
								if(r.getOffset().equals(new Point(7,0))) rtmp = r;
							}
							//balra
							else if(fields.indexOf(f2)==fields.indexOf(f)-1){
								if(r.getOffset().equals(new Point(-7,0))) rtmp = r;
							}
						}
						if(r.getStart()==f2&&r.getOffset().equals(new Point(0,0))){
							rtmp2 = r;
						}
						if (r.getStart() == start &&r.getOffset().equals(new Point(0,0))){
							//routes.add(r);
							startSegment = r;
						}
					}
					if(rtmp!=null)rtmp.addNext(rtmp2);
					
				}
			}
			try {
				br.close();
			} catch (IOException e) {
				return 1;
			}
		} catch (FileNotFoundException e) {
			return 1;
		} catch (IOException e) {
			return 1;
		}
		return 0;
	}
	/**
	 * Az els� �tszegmens visszaad�sa.
	 * Az ellens�gek gener�l�s�nal haszn�latos.
	 * @see soft4.CreepList#initialize()
	 * @return a megfelel� �tszegmens, vagy <code>null</code> ha nincs ilyen elem
	 */
	public RouteSegment getStart(){
		return startSegment;
	}
	
	
	public ArrayList<Field> getMap(){
		return fields;
	}
	
	/**
	 * 
	 * @return p�lyam�ret
	 */
	public int getSize(){
		return size;
	}
	
	/**
	 * 
	 * @return a hegy mezeje
	 */
	public Field getMountain(){
		return hegy;
	}
}
