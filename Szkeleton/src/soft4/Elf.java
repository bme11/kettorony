package soft4;
/**
 * T�nde, ellens�gt�pus
 * @author Povazson
 *
 */
public class Elf extends Creep {
	/**
	 * Konstruktor, az �s megval�s�tja.
	 * @see soft4.Creep#Creep(CreepList, RouteSegment)
	 */
	public Elf(int hp, CreepList c, RouteSegment r,int tts) {
		super(hp, c, r, tts);
	}

	/**
	 * A t�nde sebz�d�se.
	 * Ha meghal, regisztr�lja mag�t halottk�nt a list�j�n.
	 * Ha oszt�dik, nem sebz�dik, hanem megfelezi az �let�t, �s gener�l egy �j ellens�get.
	 * @param damage az alap sebz�s
	 * @param ammo a sebz�sm�dos�t� k� referenci�ja
	 */
	@Override
	public void hurt(int damage, DamageModifierStone ammo) {
		boolean splitting = (Math.random()*100>85);
		if(splitting){
			hp=(int)Math.ceil(hp/2);
			Elf tmp = clone();
			context.addCreep(tmp);
			activeSegment.addCreep(tmp);
		}
		else{
			hp-=damage*ammo.damageModifierOnDwarf();
			if(hp<=0) {
				activeSegment.removeCreep(this);
				context.imDead(this);
			}
		}
	}

	@Override
	public Elf clone(){
		Elf tmp = new Elf(hp, context,activeSegment,nextMoveIn);
		return tmp;
	}
	public String getPicture(){
		return "elf.png";
	}
	
	public int getMana(){
		return 2;
	}
}
