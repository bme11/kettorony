package soft4;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * A szüneteltetés idején megjelenő felület
 * @author Povazson
 *
 */
public class PausePanel extends JPanel implements ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5624157043093597845L;
	private Player player;
	private JButton jb_resume, jb_new, jb_score;
	private MainFrame mf;
	
	public PausePanel(Player p,MainFrame mf){
		super();
		player = p;
		this.mf = mf;
		setBackground(Color.BLACK);
		setSize(512+256,512);
		setLayout(null);
		
		jb_resume = new JButton("Resume");
		jb_resume.setBounds(256, 110, 256, 64);
		jb_resume.addActionListener(this);
		
		jb_new = new JButton("New Game");
		jb_new.setBounds(256, 224, 256, 64);
		jb_new.addActionListener(this);
		
		jb_score = new JButton("HI-Score");
		jb_score.setBounds(256, 338, 256, 64);
		jb_score.addActionListener(this);
		
		add(jb_resume);
		add(jb_new);
		add(jb_score);
		setVisible(true);
	}

	@Override 
	public Dimension getPreferredSize(){
		return new Dimension(512+256,512);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton b = (JButton)e.getSource();
		if(b==jb_resume){
			mf.hidePausePanel();
			player.resume();
		}
		else if (b==jb_new){
			player.newGame();
			mf.newGame();
		}
		else if (b==jb_score){
			JOptionPane.showMessageDialog(mf,Integer.toString(player.getHighScoreFromFile()));
		}
	}
}