package soft4;

import java.util.ArrayList;



/**
 * 
 * Player �ltal �p�thet�, buildsite-ra, t�bbf�le k� rakhat� bele, amik m�dos�tanak 1-1 alap�rt�ket.
 *  Ismeri az �t tartalmaz� buildsite-ot, valamint az �ltala el�rhet� routesegment-eket.
 *  Tud l�ni, meghat�rozott id�k�z�nk�nt, az el�rhet� creep-ek k�z�l 1-re, amit a legfontosabbnak gondol.
 *
 */
public class Tower {
	public static final int cost = 50;
	/**
	 * a tower sebz�se.
	 */
	private int damage;
	/**
	 * a torony sebz�sm�dos�t� "l�ved�ke"
	 */
	private DamageModifierStone damageModifier;
	/**
	 * a Buildsite, amin a torony �ll.
	 */
	private BuildSite field;
	/**
	 * a nextfirein alap�rt�ke.
	 */
	private int fireRate;
	/**
	 * a k�vetkez� l�v�s ideje.
	 */
	private int nextFireIn;
	/**
	 * T�vols�g, amin bel�li creep-eket tud sebezni. A t�vols�g a tower-t tartalmaz� Buildsite-t�l �rtend�,
	 * "szomsz�dban" sz�molva, teh�t pl. egy 1-es range, egy, Tower k�z�ppont�, 3x3-as n�gyzetet hat�roz meg
	 * (a Buildsite �s a szomsz�dai) , egy 2-e pedig egy 5x5-t (a Buildsite �s a szomsz�dai �s a szomsz�dok szomsz�dai).	
	 */
	private int range;
	/**
	 * A tower �ltal �p�tett lista (fel�p�t�skor �s range n�vel�skor): a range �ltal megadott Field-ekt�l lek�ri a
	 * tartalmazott routesegment-eket, ezeket pedig priorit�s alapj�n cs�kken� sorba rendezi, ezt tartalmazza a routesegments lista.
	 * private ArrayList<BoostStone> stones;
	 * a toronyba rakott k�vekb�l k�pzett azonos�t�, amib�l eld�nthet�, hogy a tower milyen t�pus� k�vekkel rendelkezik �s
	 * t�puson bel�l milyen alt�pussal (ha van ilyen).
	 */
	private ArrayList<RouteSegment> routeSegments;

	/**
	 * a jav�t� k�vek referenci�i
	 */
	private ArrayList<BoostStone> stones;
	
	private String information = "", antiInfo = "";
	
	/**
	 * csak a picture lek�rdez�shez
	 */
	public Tower(){
		
	}

	/**
	 * Konstruktor.	
	 */
	public Tower(BuildSite bsite, int dmg, int frate, int rang) {
		stones = new ArrayList<>();
		fireRate = frate;
		damage = dmg;
		range = rang;
		field = bsite;
		routeSegments = new ArrayList<>();

		for(Field f: field.neighbours){
			for(RouteSegment rs: f.routeSegments){
				if(!routeSegments.contains(rs))
					routeSegments.add(rs);
			}
		}

		//rendez�s maximumkiv�laszt�ssal (nem nagy a halmaz, nem lesz t�l lass�)
		int maxInd;
		for(int i = 0;i<routeSegments.size();i++){
			maxInd = i;
			for(int j = i+1;j<routeSegments.size();j++){
				if(routeSegments.get(j).getPriority()>routeSegments.get(maxInd).getPriority()){
					maxInd = j;
				}
			}
			RouteSegment tmp = routeSegments.get(i);
			routeSegments.set(i, routeSegments.get(maxInd));
			routeSegments.set(maxInd, tmp);
		}
	}
	
	/**
	 * Tornyokba helyezett k�vek list�ja
	 * @param info: A toronyba aktu�lisan beker�l� boost k� neve
	 */
	
	public void setInformation(String info){
		information+=info;
	}
	
	/**
	 * A toronyba behelyez�sre ker�lt k�vek list�j�t tartalmaz� string lek�r�se
	 * @return
	 */
	
	public String getInformation(){
		return information+antiInfo;
	}

	/**
	 * Attrib�tum m�dis�t� k� berak�sa, ha m�g nincs elt�rolva a kapott p�ld�ny a stones list�ban.	
	 * @param bs a hozz�rendelend� boostone p�ld�ny
	 * @return <code>true</code>, ha siker�lt berakni, <code>false</code>, ha nem
	 */
	public boolean addBoostStone(BoostStone bs){
		if(stones.contains(bs)){
			return false;
		}
		else {
			stones.add(bs);
			bs.setTowerAttribute(this);
			return true;
		}
	}

	/**
	 * Sebz�sm�dos�t� k� berak�sa, fel�l�rjuk a kapott param�terrel az eddigi �rt�ket. 	
	 * @param dmgs a hozz�rendelend� damagemodifierstone p�ld�ny
	 * @return <code>true</code>, ha siker�lt berakni, <code>false</code>, ha nem
	 */
	public boolean addDamageModifierStone(DamageModifierStone dmgs){
		damageModifier = dmgs;
		antiInfo = dmgs.getInfo();
		return true;
	}

	/**
	 * a nextfirein cs�kkent�se 1-el, ha 0 lett, �j �rt�k be�ll�t�sa a firerate alapj�n,
	 *  majd c�lpont keres�se a routesegments list�ban, az els� megtal�lt creep-n�l megh�vja a Hurt() met�dust. 	
	 */
	public void fire(){
		nextFireIn--;
		if(nextFireIn<=0){
			nextFireIn = fireRate;
			Creep c=null;
			boolean found = false;
			for(RouteSegment rs : routeSegments){
				if(!found)c = rs.getCreep();
				if(c!=null) found = true;
			}
			if(found) {
				c.hurt(damage,damageModifier);
			}
		}
	}

	/**
	 * Damage attrib�tum be�ll�t�sa.
	 * @param dmg az �j damage �rt�k
	 */
	public void setDamage(int dmg){
		damage += dmg;
	}

	/**
	 * Firerate attrib�tum be�ll�t�sa.	
	 * @param rof az �j firerate �rt�k
	 */
	public void setFireRate(int rof){
		fireRate /= rof;
	}

	/**
	 * Range attrib�tum be�ll�t�sa	
	 * @param r az �j range �rt�k
	 */
	public void setRange(int r){
		range = r;
		for(Field f: field.neighbours){
			if(range == 2){
				for(Field f2: f.neighbours){
					for(RouteSegment rs: f2.routeSegments){
						if(!routeSegments.contains(rs))
							routeSegments.add(rs);
					}
				}	
			}
			else
				for(RouteSegment rs: f.routeSegments){
					if(!routeSegments.contains(rs))
						routeSegments.add(rs);
				}
		}
		//rendez�s minimumkiv�laszt�ssal (nem nagy a halmaz, nem lesz t�l lass�)
		int maxInd;
		for(int i = 0;i<routeSegments.size();i++){
			maxInd = i;
			for(int j = i+1;j<routeSegments.size();j++){
				if(routeSegments.get(j).getPriority()<routeSegments.get(maxInd).getPriority()){
					maxInd = j;
				}
			}
			RouteSegment tmp = routeSegments.get(i);
			routeSegments.set(i, routeSegments.get(maxInd));
			routeSegments.set(maxInd, tmp);
		}
	}
	
	public String getPicture(){
		return "tower.png";
	}
	
	public Field getLocation(){
		return field;
	}
}
