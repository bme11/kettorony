package soft4;

/**
 * L�v�si sebess�g n�vel� k� alaposzt�lya 
 *
 */
public class SpeedStone extends BoostStone {

	/** Torony l�v�si sebess�g�nek m�dos�t�sa.
	 * @param t a torony p�ld�ny, amibe a k� ker�lt	
	 */
	@Override
	public void setTowerAttribute(Tower t) {
		t.setFireRate(2);
		t.setInformation(" Speed+");
	}
	
	public String getPicture(){
		return "speeds.png";
	}
}
