package soft4;

import java.io.*;
import java.util.ArrayList;

import javax.swing.JOptionPane;

/**
 * A szoftvert haszn�l� j�t�kosnak megfelel� objektum, aminek feladata a mana t�rol�sa �s m�dos�t�sa,
 *  a �k�v�lr�l� �rkez� parancsok beilleszt�se a rendszer folyamataiba.
 *
 */
public class Player {
	/**
	 * a j�t�kos �ltal kiv�lasztott field p�ld�ny.
	 */
	private Field activeField;
	/**
	 * a j�t�kos rendelkez�sre �ll� er�forr�sa, a var�zser� sz�m�rt�ke.
	 */
	private int mana;
	/**
	 * l�that�s�g miatt t�rolva
	 */
	private MapData mapData;
	/**
	 * l�that�s�g miatt t�rolva
	 */
	private Timer timer;
	/**
	 * l�that�s�g miatt t�rolva
	 */
	private TowerList towerList;
	/**
	 * l�that�s�g miatt t�rolva
	 */
	private CreepList creepList;
	
	
	private ArrayList<RoadBlock> roadBlocks;
	
	private boolean gameEnd;
	
	/**
	 * Konstruktor
	 * @param pmapdata mapdata p�ld�ny
	 * @param ptowerlist towerlist p�ld�ny
	 * @param pcreeplist creeplist p�ld�ny
	 * @param ptimer timer p�ld�ny
	 */
	public Player(MapData pmapdata,TowerList ptowerlist,CreepList pcreeplist,Timer ptimer) {
		activeField = null;
		mana = 100;
		mapData = pmapdata;
		towerList = ptowerlist;
		creepList = pcreeplist;		
		timer = ptimer;
		roadBlocks = new ArrayList<>();
		gameEnd = false;
	}
	
	/**
	 * Akad�ly �p�t�se az activefield-re, ha van r� el�g mana.	
	 */
	public void buildRoadBlock(){
		if(activeField==null) return;
		if(mana>=RoadBlock.cost){
			if(activeField.buildRoadBlock(roadBlocks)){
				mana-=RoadBlock.cost;
			}
		}
	}

	/**
	 * Torony �p�t�se az activefield-re, ha van r� el�g mana.	
	 */
	public void buildTower(int dmg, int firerate, int range){
		if(activeField==null) return;
		if(mana>=Tower.cost){
			if(activeField.buildTower(dmg, firerate, range)){
				mana-=Tower.cost;
			}
		}
	}
	
	/**
	 * A mana-hoz hozz�adja a param�tert.	
	 * @param value mana v�ltoztat�s�nak m�rt�ke: - cs�kkent, + n�vel
	 */
	public void changeMana(int value){
		mana+=value;
	}

	public int getMana(){
		return mana;
	}	
	
	/**
	 * sebz�sm�dos�t� k� rak�sa toronyba, ha van r� el�g mana.	
	 * @param mdstn a j�t�kos �ltal kiv�lasztott sebz�sm�dos�t� k� p�ld�ny. 
	 */
	public boolean damageModifierStoneToTower(DamageModifierStone mdstn){
		if(activeField!=null){
			if(mana>=mdstn.cost){
				if(activeField.addDamageModifierStoneToTower(mdstn)){
					mana-=mdstn.cost;
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * J�t�k v�ge, id�z�t� le�ll�t�sa.	
	 */
	public void defeat(){
		timer.set(0);
		gameEnd = true;
		JOptionPane.showMessageDialog(null, "�n vesztett, a v�gzet hegye elesett!!!");
	}
		
	/**
	 * �j j�t�k ind�t�sa, aktu�lis j�t�k le�ll�t�sa, majd t�rl�s �s �jragener�l�s.
	 */
	public void newGame(){
		timer.set(0);
		towerList.deleteAll();
		mana = 100;
		activeField = null;
		mapData.initialize("map_data.dat","route_data.dat","adjacency.dat");
		creepList.initialize();
		roadBlocks.clear();
		gameEnd = false;
		timer.set(12);
	}

	/**
	 * A param�ter hozz�rendel�se az activefield attrib�tumhoz.	
	 * @param field a j�t�kos �ltal kiv�lasztott field p�ld�ny
	 */
	public void setActiveField(Field field){
		activeField = field;
	}

	/**
	 * K� rak�sa akad�lyba, ha van r� el�g mana.	
	 * @param rbs a j�t�kos �ltal kiv�lasztott roadblockstone p�ld�ny, amit az akad�lyhoz rendel�nk.
	 */
	public boolean stoneToRoadBlock(RoadBlockStone rbs){
		if(activeField!=null){
			if(mana>=rbs.cost){
				if(activeField.addStoneToRoadBlock(rbs)){
					mana-=rbs.cost;
					return true;
				}
			}
		}
		return false;		
	}
	
	/**
	 * Attrib�tumm�dos�t� k� rak�sa toronyba, ha van r� el�g mana.	
	 * @param bststn a j�tt�kos �ltal kiv�lasztott booststone p�ld�ny, amit a toronyhpz rendel�nk.
	 */
	public boolean stoneToTower(BoostStone bststn){
		if(activeField!=null){
			if(mana>=bststn.cost){
				if(activeField.addBoostStoneToTower(bststn)){
					mana-=bststn.cost;
					return true;
				}
			}
		}
		return false;		
	}

	/**
	 * J�t�k v�ge, id�z�t� be�ll�t�sa, aktu�lis mana hasonl�t�sa a rekord �rt�k�hez,
	 *  ha nagyobb, �j rekord be�ll�t�sa.	
	 */
	public void victory(){
		timer.set(0);
		gameEnd = true;
		setHighScoreToFile();
		JOptionPane.showMessageDialog(null, "�n NYERT!!!");
	}
	
	public MapData getMapData(){
		return mapData;
	}

	public void pause(){
		timer.set(0);
	}
	public void resume(){
		if(!gameEnd)
			timer.set(12);
	}
	
	public int getRoadBlockNumber(){
		return roadBlocks.size();
	}
	
	public RoadBlock getRoadBlock(int sorsz){
		return roadBlocks.get(sorsz);
	}
	
	public void setHighScoreToFile(){
		if(mana>getHighScoreFromFile()){
			
			FileWriter fw = null;
			BufferedWriter bw = null;
			
			try {
			    fw = new FileWriter("highscore.txt");
				bw = new BufferedWriter(fw);
				
				bw.write(Integer.toString(mana));
			}	
				 catch (IOException e) {
					System.out.println("Hiba a f�jlkezel�s k�zben!");
					
			}finally{
				try{
					if (bw!=null)
						bw.close();
				}
				catch (IOException e){
					System.out.println("Hiba f�jlkezel�s k�zben, nem tudtam bez�rni a f�jlt!");
				}
			}	
		}
	}
	
	public int getHighScoreFromFile(){
		
		FileReader fr = null;
		BufferedReader br = null;
		int presentHighestMana = 0;
		
		try {
		    fr = new FileReader("highscore.txt");
			br = new BufferedReader(fr);
			
			
				presentHighestMana = Integer.parseInt(br.readLine());
				
		}	
			 catch (NumberFormatException e) {
				System.out.println("Nem megfelel� sz�mform�tum");
				
			} catch (IOException e) {
				System.out.println("Hiba a f�jlkezel�s k�zben!");
		}finally{
			try{
				if (br!=null)
					br.close();
			}
			catch (IOException e){
				System.out.println("Hiba f�jlkezel�s k�zben, nem tudtam bez�rni a f�jlt!");
			}
	}
		return presentHighestMana;
	}
	
	/**
	 * A kiv�laszott ter�letr�l inform�ci� szerz�se a j�t�kos fel�(toronyba helyezett k�vek)
	 * @return
	 */
	
	public String ShowTowerInfo(){
		if(activeField!=null){
			return activeField.ShowInfo();
		}
		else 
			return "Nincs info";
	}
	
	public String ShowWeatherInfo(){
		return timer.getFoggySignal();
	}
}
