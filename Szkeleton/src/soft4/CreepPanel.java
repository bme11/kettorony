package soft4;

import java.awt.Color;
import java.awt.Graphics;
import java.util.HashMap;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * A creepek mozg�s�nak panele
 * @author Povazson
 *
 */
public class CreepPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5438236436409284509L;
	/**
	 * k�pek �s a neveik map-e
	 */
	private HashMap<String,ImageIcon> pictures;
	/**
	 * a megjelen�tend� creepek
	 */
	private CreepList creepList;
	
	public CreepPanel(CreepList cl){
		super();
		creepList = cl;
		Color bckgrnd = new Color(255,0,0,0);
		setBackground(bckgrnd);
		setOpaque(false);
		setSize(512,512);
		
		pictures = new HashMap<>();
	}
	
	/**
	 * a k�p friss�t�se
	 */
	public void update(){
		removeAll();
		int max = creepList.getCreepNumber();
		if (max>10){
			max = creepList.getCreepNumber();
		}
		for (int i=0; i<max; i++){
			Creep tempcreep = creepList.getCreep(i);
			if (tempcreep!=null) {
				if(!pictures.containsKey(tempcreep.getPicture())){
					pictures.put(tempcreep.getPicture(), new ImageIcon(tempcreep.getPicture()));
				}
				JLabel tmp = new JLabel(pictures.get(tempcreep.getPicture()));
				int x = tempcreep.getLocation().getStart().x*32+tempcreep.getLocation().getOffset().x*4;
				int y = tempcreep.getLocation().getStart().y*32+tempcreep.getLocation().getOffset().y*4;
				tmp.setBounds(x,y,32,32);
				add(tmp);			
			}
		}
	}
	
	@Override
	public void paintComponent(Graphics g){
		update();
		super.repaint();
	}
	
}
