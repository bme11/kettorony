package soft4;

/**
 * Duplasebz�st ad t�rp�k ellen
 * @author Povazson
 *
 */
public class AntiDwarfDamageModifierStone extends DamageModifierStone{
	/**
	 * T�rp�k elleni b�nuszsebz�s
	 * @return a sebz�s m�dos�t�si szorz� t�rp�k ellen
	 */
	public int damageModifierOnDwarf(){
		return 2;
	}
	/**
	 * Emberek elleni b�nuszsebz�s
	 * @return a sebz�s m�dos�t�si szorz� emberek ellen
	 */
	public int damageModifierOnHuman(){
		return 1;
	}
	/**
	 * T�nd�k elleni b�nuszsebz�s
	 * @return a sebz�s m�dos�t�si szorz� t�nd�k ellen
	 */
	public int damageModifierOnElf(){
		return 1;
	}
	/**
	 * Hobbitok elleni b�nuszsebz�s
	 * @return a sebz�s m�dos�t�si szorz� hobbitok ellen
	 */
	public int damageModifierOnHobbit(){
		return 1;
	}
	
	/**
	 * k�p neve
	 */
	public String getPicture(){
		return "aDwarf.png";
	}
	
	/**
	 * kijelz�sre
	 */
	public String getInfo(){
		return "<br>antiDwarf";
	}
}
