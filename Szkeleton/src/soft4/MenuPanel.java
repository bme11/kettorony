package soft4;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

/**
 * A j�t�kot ir�ny�t� gombok �s a felhaszn�l�t �rtes�t� fel�let
 * @author Povazson
 *
 */
public class MenuPanel extends JPanel implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1076523241282703443L;
	private JPanel jp_mana, jp_build, jp_perma, jp_boost, jp_slow;
	private JButton jb_pause, jb_slow, jb_torony, jb_block;
	private JButton jb_speed, jb_range, jb_damage;
	private JButton jb_aDwarf, jb_aElf, jb_aHuman, jb_aHobbit;
	private Player player;
	private CreepList crl;
	private JLabel label_mana = new JLabel("Mana: 0");
	private JLabel jl_towerInfo = new JLabel();
	
	//BoostStone p�ld�nyok a tornyok k�pess�geinek n�vel�s�re
	private BoostStone damageBs = new DamageStone();
	private BoostStone rangeBs = new RangeStone();
	private BoostStone speedBs = new SpeedStone();
	private MainFrame mf;
	
	public MenuPanel(Player p,CreepList cl,MainFrame mf){
		super();
		this.mf = mf;
		player = p;
		crl=cl;
		setSize(256,512);
		ImageIcon icon = new ImageIcon("placeholder.png");
		
		//mana+pausegomb
		jp_mana = new JPanel();
		jp_mana.setBackground(Color.GRAY);
		
		//tower information label
		jl_towerInfo.setForeground(Color.BLUE);
		
		label_mana.setForeground(Color.BLUE);
		jp_mana.add(label_mana);
		jp_mana.add(jl_towerInfo);
		
		//BUILDABLE
		jp_build = new JPanel();
		icon = new ImageIcon(new Tower().getPicture());
		jb_torony = new JButton(icon);
		jb_torony.setContentAreaFilled(false);
		jb_torony.setPreferredSize(new Dimension(icon.getIconWidth(),icon.getIconHeight()));
		jb_torony.addActionListener(this);
		jb_torony.setToolTipText("Torony �p�t�se");
		icon = new ImageIcon(new RoadBlock().getPicture());
		jb_block = new JButton(icon);
		jb_block.setContentAreaFilled(false);
		jb_block.setPreferredSize(new Dimension(icon.getIconWidth(),icon.getIconHeight()));
		jb_block.addActionListener(this);
		jb_block.setToolTipText("�ttorlasz �p�t�se");
		
		JLabel jl_tower = new JLabel(Integer.toString(Tower.cost));
		jl_tower.setForeground(Color.BLUE);
		JLabel jl_block = new JLabel(Integer.toString(RoadBlock.cost));
		jl_block.setForeground(Color.BLUE);
		
		jp_build.add(jb_torony);
		jp_build.add(jl_tower);
		jp_build.add(jb_block);
		jp_build.add(jl_block);
		jp_build.setBackground(Color.GRAY);
		
		TitledBorder border1 = BorderFactory.createTitledBorder("Build");
		border1.setTitleColor(Color.WHITE);
		jp_build.setBorder(border1);
		
		//TowerStones
		jp_perma = new JPanel();
		icon = new ImageIcon(new SpeedStone().getPicture());
		jb_speed = new JButton(icon);
		jb_speed.setPreferredSize(new Dimension(icon.getIconWidth(),icon.getIconHeight()));
		jb_speed.setContentAreaFilled(false);
		jb_speed.addActionListener(this);
		jb_speed.setToolTipText("Torony l�v�si gyakoris�g�t jav�t� k�");
		icon = new ImageIcon(new RangeStone().getPicture());
		jb_range = new JButton(icon);
		jb_range.setContentAreaFilled(false);
		jb_range.setPreferredSize(new Dimension(icon.getIconWidth(),icon.getIconHeight()));
		jb_range.addActionListener(this);
		jb_range.setToolTipText("Torony hat�t�vols�g�t n�vel� k�");
		icon = new ImageIcon(new DamageStone().getPicture());
		jb_damage = new JButton(icon);
		jb_damage.setContentAreaFilled(false);
		jb_damage.setPreferredSize(new Dimension(icon.getIconWidth(),icon.getIconHeight()));
		jb_damage.addActionListener(this);
		jb_damage.setToolTipText("Torony sebz�s�t n�vel� k�");
		
		JLabel jl_speed = new JLabel(Integer.toString(SpeedStone.cost));
		jl_speed.setForeground(Color.BLUE);
		JLabel jl_range = new JLabel(Integer.toString(RangeStone.cost));
		jl_range.setForeground(Color.BLUE);
		JLabel jl_damage = new JLabel(Integer.toString(DamageStone.cost));
		jl_damage.setForeground(Color.BLUE);
		
		jp_perma.add(jb_speed);
		jp_perma.add(jl_speed);
		jp_perma.add(jb_range);
		jp_perma.add(jl_range);
		jp_perma.add(jb_damage);
		jp_perma.add(jl_damage);
		
		jp_perma.setBackground(Color.GRAY);
		
		TitledBorder border2 = BorderFactory.createTitledBorder("TowerStones");
		border2.setTitleColor(Color.WHITE);
		jp_perma.setBorder(border2);
		
		//Ammo
		jp_boost = new JPanel();
		
		icon = new ImageIcon(new AntiDwarfDamageModifierStone().getPicture());
		jb_aDwarf = new JButton(icon);
		jb_aDwarf.setPreferredSize(new Dimension(icon.getIconWidth(),icon.getIconHeight()));
		jb_aDwarf.setContentAreaFilled(false);
		jb_aDwarf.addActionListener(this);
		jb_aDwarf.setToolTipText("T�rp�k elleni b�nuszsebz�s");
		icon = new ImageIcon(new AntiElfDamageModifierStone().getPicture());
		jb_aElf = new JButton(icon);
		jb_aElf.setContentAreaFilled(false);
		jb_aElf.setPreferredSize(new Dimension(icon.getIconWidth(),icon.getIconHeight()));
		jb_aElf.addActionListener(this);
		jb_aElf.setToolTipText("T�nd�k elleni b�nuszsebz�s");
		icon = new ImageIcon(new AntiHobbitDamageModifierStone().getPicture());
		jb_aHobbit = new JButton(icon);
		jb_aHobbit.setContentAreaFilled(false);
		jb_aHobbit.setPreferredSize(new Dimension(icon.getIconWidth(),icon.getIconHeight()));
		jb_aHobbit.addActionListener(this);
		jb_aHobbit.setToolTipText("Hobbitok elleni b�nuszsebz�s");
		icon = new ImageIcon(new AntiHumanDamageModifierStone().getPicture());
		jb_aHuman = new JButton(icon);
		jb_aHuman.setContentAreaFilled(false);
		jb_aHuman.setPreferredSize(new Dimension(icon.getIconWidth(),icon.getIconHeight()));
		jb_aHuman.addActionListener(this);
		jb_aHuman.setToolTipText("Emberek elleni b�nuszsebz�s");
		
		JLabel jl_aDwarf = new JLabel(Integer.toString(AntiDwarfDamageModifierStone.cost));
		jl_aDwarf.setForeground(Color.BLUE);
		JLabel jl_aElf = new JLabel(Integer.toString(AntiElfDamageModifierStone.cost));
		jl_aElf.setForeground(Color.BLUE);
		JLabel jl_aHobbit = new JLabel(Integer.toString(AntiHobbitDamageModifierStone.cost));
		jl_aHobbit.setForeground(Color.BLUE);
		JLabel jl_aHuman = new JLabel(Integer.toString(AntiHumanDamageModifierStone.cost));
		jl_aHuman.setForeground(Color.BLUE);
		
		jp_boost.add(jb_aDwarf);
		jp_boost.add(jl_aDwarf);
		jp_boost.add(jb_aElf);
		jp_boost.add(jl_aElf);
		jp_boost.add(jb_aHobbit);
		jp_boost.add(jl_aHobbit);
		jp_boost.add(jb_aHuman);
		jp_boost.add(jl_aHuman);
		
		jp_boost.setBackground(Color.GRAY);
		
		TitledBorder border3 = BorderFactory.createTitledBorder("AmmoTypes");
		border3.setTitleColor(Color.WHITE);
		jp_boost.setBorder(border3);
		
		//blockstone
		jp_slow = new JPanel();
		
		icon = new ImageIcon(new RoadBlockStone().getPicture());
		jb_slow = new JButton(icon);
		jb_slow.setPreferredSize(new Dimension(icon.getIconWidth(),icon.getIconHeight()));
		jb_slow.setContentAreaFilled(false);
		jb_slow.addActionListener(this);
		jb_slow.setToolTipText("�ttorlasz �tj�rhat�s�g�nak cs�kkent�se");
		
		JLabel jl_slow = new JLabel(Integer.toString(RoadBlockStone.cost));
		jl_slow.setForeground(Color.BLUE);
		
		jp_slow.add(jb_slow);
		jp_slow.add(jl_slow);
		
		jp_slow.setBackground(Color.GRAY);
		
		TitledBorder border4 = BorderFactory.createTitledBorder("RoadBlockStone");
		border4.setTitleColor(Color.WHITE);
		jp_slow.setBorder(border4);
		
		//PAUSE
		JPanel jp_pause = new JPanel();
		jb_pause = new JButton("Menu");
		jb_pause.addActionListener(this);
		jp_pause.add(jb_pause);
		jp_pause.setBackground(Color.GRAY);
		
		//EG�SZ
		setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		setBackground(Color.GRAY);
		add(jp_mana);
		add(jp_build);
		add(jp_perma);
		add(jp_boost);
		add(jp_slow);
		add(jp_pause);
	}
	
	@Override
	public Dimension getPreferredSize(){
		return new Dimension(256,512);
	}
	@Override
	public Dimension getMaximumSize(){
		return new Dimension(256,512);
	}

	/**
	 * A gombnyom�sok lekezel�se
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton b = (JButton)e.getSource();
		if(b == jb_torony){
			/**
			 * int dmg, int firerate, int range
			 */
			player.buildTower(1, 50, 1);
		}
		else if (b== jb_block){
			player.buildRoadBlock();
		}
		else if (b == jb_damage){
			player.stoneToTower(damageBs);
		}
		else if (b == jb_range){
			player.stoneToTower(rangeBs);
		}
		else if (b == jb_speed){
			player.stoneToTower(speedBs);
		}
		else if (b == jb_aDwarf){
			player.damageModifierStoneToTower(new AntiDwarfDamageModifierStone());
		}
		else if (b == jb_aHuman){
			player.damageModifierStoneToTower(new AntiHumanDamageModifierStone());
		}
		else if (b == jb_aElf){
			player.damageModifierStoneToTower(new AntiElfDamageModifierStone());
		}
		else if (b == jb_aHobbit){
			player.damageModifierStoneToTower(new AntiHobbitDamageModifierStone());
		}
		else if (b == jb_slow){
			player.stoneToRoadBlock(new RoadBlockStone());
		}
		else if (b == jb_pause){
			player.pause();
			mf.showPausePanel();
		}
	}
	
	/**
	 * A fenti kijelz�s friss�t�se
	 */
	public void update(){
		label_mana.setText("<html><p align = center>Mana: "+player.getMana()+"<br>Creeps remaining: "+crl.getCreepNumber()
				+"<br>Info: "+ player.ShowTowerInfo()
				+"<br>Id�j�r�s: "+ player.ShowWeatherInfo()
				+"</p></hmtl>");
		super.repaint();
	}
	
}
