package soft4;

import java.util.ArrayList;
import java.util.Random;
/**
 * Az ellens�geket tartalmaz� lista.
 * @author Povazson
 *
 */
public class CreepList {
	/**
	 * az �l� �s a m�r halott de m�g nem t�r�lt ellens�gek list�i
	 */
	private ArrayList<Creep> creeps,deads;
	/**
	 * a j�t�kos referenci�ja
	 */
	private Player player;
	
	/**
	 * Konstruktor
	 * @param pl A j�t�kos
	 * @param mp a p�lya adatai
	 */
	public CreepList(Player pl) {
		creeps = new ArrayList<Creep>();
		deads = new ArrayList<Creep>();
		player = pl;
	}
	
	/**
	 * A meghal� ellens�gek e f�ggv�nnyel regisztr�lj�k hal�lukat.
	 * @param creep az ellens�g, aki �pp meghal
	 */
	public void imDead(Creep creep){
		deads.add(creep);
	}
	
	/**
	 * A list�k t�rl�se, valamint �j ellens�gek gener�l�sa.
	 */
	public void initialize(){
		creeps.clear();
		deads.clear();
		int waves = 10;
		int wavecreeps = 1;
		Random szam = new Random(); 

		for (int i=0;i<waves;i++){
	
			wavecreeps += (i)*3+2 ;
			for (int j=0; j<wavecreeps; j++){
				switch (szam.nextInt(4)) {
					case 0: addCreep(new Dwarf(6,this,player.getMapData().getStart(),2000*i+j*10));
							break;
					case 1: addCreep(new Elf(5,this,player.getMapData().getStart(),2000*i+j*10));
							break;
					case 2: addCreep(new Human(3,this,player.getMapData().getStart(),2000*i+j*10));
							break;
					case 3: addCreep(new Hobbit(2,this,player.getMapData().getStart(),2000*i+j*10));
							break;

				}
			}
		}
	}
	
	/**
	 * Minden �l� ellens�g <code>move</code> f�ggv�ny�nek h�v�sa.
	 */
	public void moveAll(){
		if(getCreepNumber()==0){
			player.victory();
			return;}
		for(Creep c:creeps){
			c.move();
		}
	}
	
	/**
	 * A konstukt�lhat�s�ghoz
	 * @param p player
	 */
	public void setPlayer(Player p){
		player = p;
	}
	
	public Player getPlayer(){
		return player;
	}
	/**
	 * A meghalt ellens�gek kit�rl�se,
	 * valamint a j�t�kos man�j�nak n�vel�se minden halott ut�n.
	 * Ha minden �l� ellens�g meghalt, akkor a j�tlkos nyer.
	 */
	public void removeDead(){
		if (deads.size() > 0){
			for(Creep c:deads){
				player.changeMana(c.getMana());
				creeps.remove(c);
			}
			deads.clear();
		}
		if  (creeps.size() == 0){
			player.victory();
			return;
		}
	}
	
	/**
	 * creep hozz�ad�sa a list�hoz
	 * @param c a creep
	 */
	public void addCreep(Creep c){
		creeps.add(c);
	}
	
	/**
	 * 
	 * @return mennyi creep van a list�ban
	 */
	public int getCreepNumber(){
		return creeps.size();
	}
	
	public Creep getCreep(int sorsz){
		if ((sorsz<creeps.size())&&(creeps.get(sorsz).getLocation()!=player.getMapData().getStart())){
			return creeps.get(sorsz);
		}
		else {
			return null;
		}
			
		
	}

}
