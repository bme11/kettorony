package soft4;
/**
 * A torony alapsebz�s�t n�vel� k�.
 * @author Povazson
 *
 */
public class DamageStone extends BoostStone {
	/**
	 * A kapott torony sebz�s�nek be�ll�t�sa a n�velt �rt�kre.
	 * @param t A jav�tand� torony referenci�ja
	 */
	@Override
	public void setTowerAttribute(Tower t) {
		t.setDamage(2);
		t.setInformation(" Damage+");
	}
	
	public String getPicture(){
		return "dmgs.png";
	}
}
