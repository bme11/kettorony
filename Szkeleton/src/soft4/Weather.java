package soft4;

/**
 * A k�d�s�t�st v�gz� oszt�ly
 * @author Povazson
 *
 */
public class Weather {
	/**
	 * a k�d h�tral�v� id�tartama
	 * ha nincs k�d, 0
	 */
	private int foggy;
	
	private String fogSignal;
	
	/**
	 * init
	 */
	public void init(){
		foggy = 0;
	}
	
	public String getFogSignal(){
		return fogSignal;
	}
	
	/**
	 * k�d gener�l�sa ha kell, ha nem cs�kkenti 1 el a visszamarad� �rt�k�t
	 */
	public void time(){
		if(foggy ==0){
			int rnd = (int)Math.round(Math.random()*100);
			if(rnd>=99){
				foggy = (int)Math.round(Math.random()*50);
			}
			else
				fogSignal = "Tiszta id�";
		}
		else{
			foggy--;
			fogSignal = "K�d�s id�";
		}
	}
	
	/**
	 * Megadja, hogy az adott szegmens visszaadhat e creepet, vagy el vannak takarva a k�d �ltal.
	 * A l�that�s�got k�d eset�n v�letlen sz�m d�nti el.
	 * @return <code>true</code> ha nem l�that�, <code>false</code> ha nincs k�d, vagy ha l�that�k.
	 */
	public boolean covered(){
		boolean seged = false;
		if(foggy==0) return seged;
		int rnd = (int)(Math.round(Math.random()*100));
		if(rnd >=60) seged = true;
		else seged = false;
		return seged;
	}
}
