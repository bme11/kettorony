package soft4;

/**
 * Range n�vel� k� alaposzt�lya 
 *
 */
public class RangeStone extends BoostStone {

	/**
	 * Torony range-�nek m�dos�t�sa.
	 * @param t a torony p�ld�ny, amibe a k� ker�lt	
	 */
	@Override
	public void setTowerAttribute(Tower t) {
		t.setRange(2);
		t.setInformation(" Range+");
	}

	public String getPicture(){
		return "ranges.png";
	}
}
