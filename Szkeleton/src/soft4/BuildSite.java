package soft4;

import java.util.ArrayList;



/**
 * 
 * Az �p�t�si ter�let, melyekre torony �p�thet�.
 * @author Povazson
 */
public class BuildSite extends Field {
	/**
	 * A mez�n tal�lhat� torony referenci�ja, null ha nincs.
	 */
	private Tower tower;
	
	/**
	 * A tornyok list�ja, melybe a tornyot bele kell jegyezni, ha meg�p�l
	 */
	private TowerList towerList;
	
	/**
	 * Konstruktor.
	 * @param tl A toronylista, melybe majd a tornyot jegyezhetj�k referenci�ja. 
	 */
	public BuildSite(TowerList tl) {
		super();
		tower = null;
		towerList = tl;
	}
	
	/**
	 * Tornyot �p�t, ha nincs m�g rajta.
	 * @return true ha siker�lt az �p�t�s
	 * @return false ha nem siker�lt �p�teni
	 */
	
	@Override
	public boolean buildTower(int dmg, int firerate, int range) {
		if(tower==null) {
			tower = new Tower(this, dmg, firerate, range);
			tower.addDamageModifierStone(new DamageModifierStone());
			towerList.addTower(tower);
			return true;
		}
		else {
			return false;
		}
	}
	
	
	/**
	 * A k�z�s kezelhet�s�g miatt l�tezik a f�ggv�ny.
	 * @return false, �ttorlasz csak �tra �p�thet�.
	 */
	@Override
	public boolean buildRoadBlock(ArrayList<RoadBlock> list) {
		return false;		
	}
	
	/**
	 * Tulajdons�gjav�t� k� toronyba helyez�se.
	 * @param boostStone az elhelyezni k�v�nt k� referenci�ja
	 * @return {@linkplain soft4.Tower#addBoostStone(BoostStone)}  visszat�r�si �rt�ke, ha van rajta torony.
	 * @return false Ha nincs rajta torony.
	 */
	@Override
	public boolean addBoostStoneToTower(BoostStone boostStone){
		if(tower!=null){
			return tower.addBoostStone(boostStone);
		}
		else {
			return false;
		}
	}
	
	/**
	 * Sebz�sm�dos�t� k� toronyba helyez�se.
	 * @param modStone az elhelyezni k�v�nt k� referenci�ja.
	 * @return {@linkplain soft4.Tower#addDamageModifierStone(DamageModifierStone)} visszat�r�si �rt�ke, ha van rajta torony
	 * @return false Ha nincs rajta torony.
	 */
	@Override
	public boolean addDamageModifierStoneToTower(DamageModifierStone modStone){
		if(tower!=null){
			return tower.addDamageModifierStone(modStone);
		}
		else {
			return false;
		}
	}
	
	/**
	 * A k�z�s kezelhet�s�g miatt l�tezik a f�ggv�ny.
	 * @return false, csak �tra �rtelmezett.
	 */
	@Override
	public boolean addStoneToRoadBlock(RoadBlockStone stone){
		return false;
	}
	
	/**
	 * k�p neve
	 */
	public String getPicture(){
		return "build.png";
	}
	
	/**
	 * Ha van torony az adott ter�leten, akkor ki�rja a toronyba tal�lhat� k�veket
	 */
	@Override
	public String ShowInfo(){
		if(tower!=null){
		return tower.getInformation();
		}
		else
			return "Nincs torony";
	}
	
}
