package soft4;

import java.awt.Dimension;

import javax.swing.JFrame;

/**
 * 
 * @author Povazson
 *
 */
public class MainFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 934833751684055549L;
	private MenuPanel menu;
	private MapPanel map;
	private TowerPanel tower;
	private CreepPanel creeps;
	private PausePanel pause;
	
	public MainFrame(Player p, TowerList tl,CreepList cl){
		super();
		//�sszetev�k inicializ�l�sa
		menu = new MenuPanel(p, cl,this);
		map = new MapPanel(p);
		tower = new TowerPanel(tl,p);
		creeps = new CreepPanel(cl);
		pause = new PausePanel(p,this);
		
		setTitle("Kettorony");
		setLayout(null);
		setResizable(false);
		setSize(512+256, 512);
		menu.setVisible(true);
		getContentPane().add(map);
		getContentPane().add(menu);
		menu.setBounds(513,0,256,512);
		map.setBounds(0,0,512,512);
		menu.setLocation(513, 0);
		getLayeredPane().add(tower, new Integer(1));
		getLayeredPane().add(creeps,new Integer(2));
		tower.setBounds(0, 0, 512, 512);
		creeps.setBounds(0,0,512,512);
		pause.setBounds(0,0,512+256,512);
		getContentPane().setPreferredSize(new Dimension(512+256,512));
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
	}
	
	/**
	 * pausepanel a t�bbi f�l� helyez�se
	 */
	public void showPausePanel(){
		getLayeredPane().add(pause, new Integer(3));
		pause.setBounds(0,0, 512+256, 512);
	}
	
	/**
	 * rendes j�t�kt�r vissza�ll�t�sa, megtartva a szelekci�t
	 */
	public void hidePausePanel(){
		getLayeredPane().remove(pause);
		repaintAll();
	}
	
	/**
	 * rendes j�t�kt�r vissza�ll�t�sa, a szelekci�t t�r�lve
	 */
	public void newGame(){
		map.clearActive();
		hidePausePanel();
	}
	
	public void repaintAll(){
		menu.update();
		menu.repaint();
		map.repaint();
		tower.repaint();
		creeps.repaint();
		repaint();
	}
	
	public static void main(String[] args) {
		TowerList towerList = new TowerList();
		Weather weather = new Weather();
		MapData mapData = new MapData(towerList,weather);
		CreepList creepList = new CreepList(null);
		Timer timer = new Timer(null,towerList,creepList,weather);
		Player player = new Player(mapData,towerList,creepList,timer);
		MainFrame mf = new MainFrame(player, towerList,creepList);	
		timer.mf = mf;
		timer.setPlayer(player);
		creepList.setPlayer(player);
		creepList.initialize();
		timer.set(12);
		timer.run();
	}

}
