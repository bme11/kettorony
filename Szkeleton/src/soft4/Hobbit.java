package soft4;

/**
 * Hobbit, ellens�gt�pus.
 * @author Povazson
 *
 */
public class Hobbit extends Creep {
	/**
	 * Konstruktor, az �s megval�s�tja.
	 * @see soft4.Creep#Creep(CreepList, RouteSegment)
	 */
	public Hobbit(int hp, CreepList c, RouteSegment r,int tts) {
		super(hp, c, r, tts);
	}
	
	/**
	 * A hobbit sebz�d�se.
	 * Ha meghal, regisztr�lja mag�t halottk�nt a list�j�n.
	 * Ha oszt�dik, nem sebz�dik, hanem megfelezi az �let�t, �s gener�l egy �j ellens�get.
	 * @param damage az alap sebz�s
	 * @param ammo a sebz�sm�dos�t� k� referenci�ja
	 */
	@Override
	public void hurt(int damage, DamageModifierStone ammo) {
		boolean splitting = (Math.random()*100>85);
		if(splitting){
			hp=(int)Math.ceil(hp/2);
			Hobbit tmp = clone();
			context.addCreep(tmp);
			activeSegment.addCreep(tmp);
		}
		else{
			hp-=damage*ammo.damageModifierOnHobbit();
			if(hp<=0) {
				activeSegment.removeCreep(this);
				context.imDead(this);
			}
		}
	}

	@Override
	public Hobbit clone(){
		Hobbit tmp = new Hobbit(hp, context,activeSegment,nextMoveIn);
		return tmp;
	}
	public String getPicture(){
		return "hobbit.png";
	}
	
	public int getMana(){
		return 1;
	}

}
